nll_bayesian_confidence = function(noise.sigmaLow, extraNoise, crit_Low, crit_High, noiseFact, subjData, data_LowNoise_S1, data_LowNoise_S2, data_HighNoise_S1, data_HighNoise_S2, alts_low, alts_high) {
  library(here)
  library(tidyr)
  
  #Uncomment this for using measured ratio between noise levels
  #NOTE: if you do this, no longer fitting extraNoise, so this can be taken out of optimization
  #noise.sigmaHigh = noise.sigmaLow*noiseFact
  
  noise.sigmaHigh = noise.sigmaLow+extraNoise
  
  # Low Noise
  crit = crit_Low
  confBounds_1 = crit
  confBounds_2 = crit
  confStep_1 = 0.5/6
  confStep_2 = 0.5/6
  nextConf = 0.5
  for (i in 2:6){
    nextConf = nextConf + confStep_2
    x = qnorm(nextConf, crit, noise.sigmaLow)
    confBounds_2 = c(confBounds_2, crit + abs(x-crit))
  }
  confBounds_2 = c(confBounds_2, Inf)
  
  nextConf = 0.5
  for (i in 2:6){
    nextConf = nextConf + confStep_1
    x = qnorm(nextConf, crit, noise.sigmaLow)
    confBounds_1 = c(confBounds_1, crit - abs(x-crit))
  }
  confBounds_1 = c(confBounds_1, -Inf)
  
  confBounds_LN = c(confBounds_1, confBounds_2)
  
  alts = alts_low
  
  # Calculate probabilities of ratings
  pr_S1C1 = rep(0, length(alts))
  pr_S1C2 = rep(0, length(alts))
  pr_S1C3 = rep(0, length(alts))
  pr_S1C4 = rep(0, length(alts))
  pr_S1C5 = rep(0, length(alts))
  pr_S1C6 = rep(0, length(alts))
  
  pr_S2C1 = rep(0, length(alts))
  pr_S2C2 = rep(0, length(alts))
  pr_S2C3 = rep(0, length(alts))
  pr_S2C4 = rep(0, length(alts))
  pr_S2C5 = rep(0, length(alts))
  pr_S2C6 = rep(0, length(alts))
  
  for (i in 1:length(alts)){
    pr_S1C1[i] = pnorm(confBounds_1[1], alts[i], noise.sigmaLow) - pnorm(confBounds_1[2], alts[i], noise.sigmaLow)
    pr_S1C2[i] = pnorm(confBounds_1[2], alts[i], noise.sigmaLow) - pnorm(confBounds_1[3], alts[i], noise.sigmaLow)
    pr_S1C3[i] = pnorm(confBounds_1[3], alts[i], noise.sigmaLow) - pnorm(confBounds_1[4], alts[i], noise.sigmaLow)
    pr_S1C4[i] = pnorm(confBounds_1[4], alts[i], noise.sigmaLow) - pnorm(confBounds_1[5], alts[i], noise.sigmaLow)
    pr_S1C5[i] = pnorm(confBounds_1[5], alts[i], noise.sigmaLow) - pnorm(confBounds_1[6], alts[i], noise.sigmaLow)
    pr_S1C6[i] = pnorm(confBounds_1[6], alts[i], noise.sigmaLow) - pnorm(confBounds_1[7], alts[i], noise.sigmaLow)
    
    pr_S2C1[i] = pnorm(confBounds_2[2], alts[i], noise.sigmaLow) - pnorm(confBounds_2[1], alts[i], noise.sigmaLow)
    pr_S2C2[i] = pnorm(confBounds_2[3], alts[i], noise.sigmaLow) - pnorm(confBounds_2[2], alts[i], noise.sigmaLow)
    pr_S2C3[i] = pnorm(confBounds_2[4], alts[i], noise.sigmaLow) - pnorm(confBounds_2[3], alts[i], noise.sigmaLow)
    pr_S2C4[i] = pnorm(confBounds_2[5], alts[i], noise.sigmaLow) - pnorm(confBounds_2[4], alts[i], noise.sigmaLow)
    pr_S2C5[i] = pnorm(confBounds_2[6], alts[i], noise.sigmaLow) - pnorm(confBounds_2[5], alts[i], noise.sigmaLow)
    pr_S2C6[i] = pnorm(confBounds_2[7], alts[i], noise.sigmaLow) - pnorm(confBounds_2[6], alts[i], noise.sigmaLow)
    
  }
  S1_probs = data.frame(C1 = pr_S1C1, C2 = pr_S1C2, C3 = pr_S1C3, C4 = pr_S1C4, C5 = pr_S1C5, C6 = pr_S1C6)
  S2_probs = data.frame(C1 = pr_S2C1, C2 = pr_S2C2, C3 = pr_S2C3, C4 = pr_S2C4, C5 = pr_S2C5, C6 = pr_S2C6)
  
  logL = 0
  
  choiceData = data_LowNoise_S1
  for (conf in 1:6){
    confData = dplyr::filter(choiceData, ConfRatings==conf)
    confData = confData[order(confData$Alterations),]
    confData$P = S1_probs[,conf]
    confData$LL = confData$count*log(confData$P)
    logL = logL + sum(confData$LL)
  }
  
  choiceData = data_LowNoise_S2
  for (conf in 1:6){
    confData = dplyr::filter(choiceData, ConfRatings==conf)
    confData = confData[order(confData$Alterations),]
    confData$P = S2_probs[,conf]
    confData$LL = confData$count*log(confData$P)
    logL = logL + sum(confData$LL)
  }
    
  # High Noise

  crit = crit_High
  confBounds_1 = crit
  confBounds_2 = crit
  confStep_1 = 0.5/6
  confStep_2 = 0.5/6
  nextConf = 0.5
  for (i in 2:6){
    nextConf = nextConf + confStep_2
    x = qnorm(nextConf, crit, noise.sigmaHigh)
    confBounds_2 = c(confBounds_2, crit + abs(x-crit))
  }
  confBounds_2 = c(confBounds_2, Inf)
  
  nextConf = 0.5
  for (i in 2:6){
    nextConf = nextConf + confStep_1
    x = qnorm(nextConf, crit, noise.sigmaHigh)
    confBounds_1 = c(confBounds_1, crit - abs(x-crit))
  }
  confBounds_1 = c(confBounds_1, -Inf)
  
  confBounds_HN = c(confBounds_1, confBounds_2)
  alts = alts_high
  
  # Calculate probabilities of ratings
  pr_S1C1 = rep(0, length(alts))
  pr_S1C2 = rep(0, length(alts))
  pr_S1C3 = rep(0, length(alts))
  pr_S1C4 = rep(0, length(alts))
  pr_S1C5 = rep(0, length(alts))
  pr_S1C6 = rep(0, length(alts))
  
  pr_S2C1 = rep(0, length(alts))
  pr_S2C2 = rep(0, length(alts))
  pr_S2C3 = rep(0, length(alts))
  pr_S2C4 = rep(0, length(alts))
  pr_S2C5 = rep(0, length(alts))
  pr_S2C6 = rep(0, length(alts))
  
  for (i in 1:length(alts)){
    pr_S1C1[i] = pnorm(confBounds_1[1], alts[i], noise.sigmaHigh) - pnorm(confBounds_1[2], alts[i], noise.sigmaHigh)
    pr_S1C2[i] = pnorm(confBounds_1[2], alts[i], noise.sigmaHigh) - pnorm(confBounds_1[3], alts[i], noise.sigmaHigh)
    pr_S1C3[i] = pnorm(confBounds_1[3], alts[i], noise.sigmaHigh) - pnorm(confBounds_1[4], alts[i], noise.sigmaHigh)
    pr_S1C4[i] = pnorm(confBounds_1[4], alts[i], noise.sigmaHigh) - pnorm(confBounds_1[5], alts[i], noise.sigmaHigh)
    pr_S1C5[i] = pnorm(confBounds_1[5], alts[i], noise.sigmaHigh) - pnorm(confBounds_1[6], alts[i], noise.sigmaHigh)
    pr_S1C6[i] = pnorm(confBounds_1[6], alts[i], noise.sigmaHigh) - pnorm(confBounds_1[7], alts[i], noise.sigmaHigh)
    
    pr_S2C1[i] = pnorm(confBounds_2[2], alts[i], noise.sigmaHigh) - pnorm(confBounds_2[1], alts[i], noise.sigmaHigh)
    pr_S2C2[i] = pnorm(confBounds_2[3], alts[i], noise.sigmaHigh) - pnorm(confBounds_2[2], alts[i], noise.sigmaHigh)
    pr_S2C3[i] = pnorm(confBounds_2[4], alts[i], noise.sigmaHigh) - pnorm(confBounds_2[3], alts[i], noise.sigmaHigh)
    pr_S2C4[i] = pnorm(confBounds_2[5], alts[i], noise.sigmaHigh) - pnorm(confBounds_2[4], alts[i], noise.sigmaHigh)
    pr_S2C5[i] = pnorm(confBounds_2[6], alts[i], noise.sigmaHigh) - pnorm(confBounds_2[5], alts[i], noise.sigmaHigh)
    pr_S2C6[i] = pnorm(confBounds_2[7], alts[i], noise.sigmaHigh) - pnorm(confBounds_2[6], alts[i], noise.sigmaHigh)
   
  }
  S1_probs = data.frame(C1 = pr_S1C1, C2 = pr_S1C2, C3 = pr_S1C3, C4 = pr_S1C4, C5 = pr_S1C5, C6 = pr_S1C6)
  S2_probs = data.frame(C1 = pr_S2C1, C2 = pr_S2C2, C3 = pr_S2C3, C4 = pr_S2C4, C5 = pr_S2C5, C6 = pr_S2C6)
  
  choiceData = data_HighNoise_S1
  for (conf in 1:6){
    confData = dplyr::filter(choiceData, ConfRatings==conf)
    confData = confData[order(confData$Alterations),]
    confData$P = S1_probs[,conf]
    confData$LL = confData$count*log(confData$P)
    logL = logL + sum(confData$LL)
  }
  
  choiceData = data_HighNoise_S2
  for (conf in 1:6){
    confData = dplyr::filter(choiceData, ConfRatings==conf)
    confData = confData[order(confData$Alterations),]
    confData$P = S2_probs[,conf]
    confData$LL = confData$count*log(confData$P)
    logL = logL + sum(confData$LL)
  }
  
  if (is.nan(logL)) {
    nll = Inf
  } else {
    nll = -logL
  }

  return(nll)  
  
}

